"use strict";

function isObject(obj) {
  return Object.prototype.toString.call(obj) === "[object Object]";
}

function spinnerStart(msg = "loading..", string = "|/-\\") {
  const Spinner = require("cli-spinner").Spinner;
  const spinner = new Spinner(msg + "%s");
  spinner.setSpinnerString(string);
  spinner.start();
  return spinner;
}

function spawn(command, args, options) {
  const win32 = process.platform === "win32";
  const cmd = win32 ? "cmd" : command;
  const cmdArgs = win32 ? ["/c"].concat(command, args) : args;
  return require("child_process").spawn(cmd, cmdArgs, options || {});
}

function execPromise(command, args, options) {
  return new Promise((resolve, reject) => {
    const p = spawn(command, args, options);
    p.on("errpr", (e) => {
      reject(e);
    });
    p.on("exit", (c) => {
      resolve(c);
    });
  });
}

module.exports = {
  isObject,
  spinnerStart,
  spawn,
  execPromise,
};
