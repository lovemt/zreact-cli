"use strict";
const path = require("path");

module.exports = formatPath;

function formatPath(p) {
  if (p && typeof p === "string") {
    //兼容linux maxOs windows。统一改成 /
    const sep = path.sep;
    if (sep === "/") {
      return p;
    } else {
      return p.replace(/\\/g, "/");
    }
  }
  return p;
}
