"use strict";

const log = require("npmlog");
//支持debugger模式
log.level = process.env.LOG_LEVEL ? process.env.LOG_LEVEL : "info";
// log.heading = "zrc"; //修改前缀
log.addLevel("success", 2000, { fg: "green" }); //增加自定义的指令

module.exports = log;
