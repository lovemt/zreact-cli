const request = require("@zreact-cli/request");

module.exports = function () {
  return request({
    url: "/project/template",
    method: "GET",
  });
};
