const { Command } = require("commander");
const pkg = require("../package.json");

const program = new Command();
program
  .version(pkg.version)
  .option("-d, --debug", "debug mode")
  .option("-s, --ss", "debug mode")
  .command("install [name]", "install one or more packages")
  .parse(process.argv);

program.arguments("<cmd> [options]").description("test command", {
  cmd: "command to run",
  options: "options for command",
});

// const service = program.command("service <name> [opts]");
// service
//   .alias("s")
//   .option("-f, --force", "启动服务")
//   .action((name, opts) => {
//     console.log("name", name);
//     console.log("opts", opts);
//   });

// program.addCommand(service);

module.exports = program;
