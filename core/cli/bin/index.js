#!/usr/bin/env node
const importLocal = require("import-local"); //导入的本地包
const log = require("@zreact-cli/log");
if (importLocal(__filename)) {
  log.notice("正在使用本地版本");
} else {
  require("../lib/core")(process.argv.slice(2));
}
