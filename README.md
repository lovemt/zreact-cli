## semver 版本号比对

## NODE 多进程

- spawn: 处理耗时任务，比如 npm install ，
- exec/execFile： 开销比较小的任务
- fork： 使用 NODE 来执行命令 NODE(main) -> 开辟 NODE(child) 执行 js 文件

## 重点

### SHELL 使用

方法一：直接执行 shell 文件

```
/bin/sh xx.shell
```

方法二：直接 shell 命令

```
/bin/sh -c "ls -al"
```

## 第五周

prepare 准备阶段

1. inquirer 询问是否需要清空文件夹，强制更新
2. fs-extra emptyDirSync() 清空文件夹

monogodb （--fork 后台运行 --dbpath 数据存储 --logpath 日志存放) https://www.runoob.com/mongodb/mongodb-osx-install.html
启动 mongod --dbpath /usr/local/var/mongodb --logpath /usr/local/var/log/mongodb/mongo.log --fork
